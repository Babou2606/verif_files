#!/bin/bash
#####################################################################################
# Script de vérification de fichiers
#
# v1.0
#author : B.GUILLAUD
#####################################################################################

function verif_arg() {
	if [ $arg -eq 0 ]
	then
		echo "Attention, il n'y a pas d'arguments !!! Veuillez relancer le script avec les arguments !"
		return 2
	else
		return 0
	fi
}

function verif_file(){
	test_arg=`ls -l | grep $argument` 
	if [ $? != 0 ]
	then
		echo "$argument n'existe pas !"
	else
		echo "$argument existe !"
	fi
}

for arg in $#
do
	verif_arg $#
	if [ $? != 0 ]
	then
		exit 2
	else
		for argument in $@
		do
			verif_file argument
		done
	fi
done
